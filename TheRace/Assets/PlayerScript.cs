﻿using UnityEngine;

public class PlayerScript : MonoBehaviour {

    // This is the reference to the Rigibbody compononet of the player
    public Rigidbody rb;
    public float forwardForce = 1000f;
    public float sidwaysForce = 500f;
    public bool moveRight = false;
    public bool moveLeft = false;
    public PlayerScript player;
    public Vector3 offsetRight;
    public Vector3 offsetLeft;
    int leftCount = 0;
    int rightCount = 0;
    //bool hitALeftWall = false;
    //bool hitARightWall = false;
    string groundTouch = "ground_4";

    // Use this for initialization
    void Start ()
    {
		Debug.Log ("Hello world");
        
	}

    // update for one off events since fixedupdate might run slower
    private void Update()
    {
        // checking if the left right buttons are touched or not
        if (Input.GetKey("d"))
        {
            moveRight = true;
            rightCount = rightCount + 1;
        }
        else
        {
            moveRight = false;
            rightCount = 0;
        }
        if (Input.GetKey("a"))
        {
            moveLeft = true;
            leftCount = leftCount + 1;
        }
        else
        {
            moveLeft = false;
            leftCount = 0;
        }
    }

    // Update is called once per frame
    // we marked this as "Fixed"Update because we
    // are using it to mess with physics.
    void FixedUpdate ()
    {
        // add a forward force
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        // add right and left forces
        if (moveRight)
        {

            if (rightCount == 1 & groundTouch != "ground_7")
            {
                //rb.AddForce(sidwaysForce * Time.deltaTime, 0, 0);
                transform.position = transform.position + offsetRight;
            }
        }
        if (moveLeft)
        {
            if (leftCount == 1 & groundTouch != "ground_1")
            {
                //rb.AddForce(-sidwaysForce * Time.deltaTime, 0, 0);
                transform.position = transform.position + offsetLeft;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.collider.name);
        // what happens when the player collides with an obstacle
        if(collision.collider.tag == "Obstacle")
        {
            player.enabled = false;
        }

        // marking on what ground we're on
        groundTouch = collision.collider.name;
    }
}
