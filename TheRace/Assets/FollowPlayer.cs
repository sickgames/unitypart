﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;
    public Vector3 offset;
    public new Vector3 camera;

	// Update is called once per frame
	void Update () {
        //transform.position = player.position + offset;
        camera.Set (0f, 12.8f, player.position.z + offset.z);
        transform.position = camera;
	}
}
